(setq initial-major-mode 'org-mode)

(setq-default inhibit-startup-screen t)
(setq inhibit-splash-screen t)

(setq backup-directory-alist
      `(("." . ,(expand-file-name "tmp/backups/" user-emacs-directory))))

(make-directory (expand-file-name "tmp/auto-saves/" user-emacs-directory) t)

(setq auto-save-list-file-prefix
      (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory)
      auto-save-file-name-transforms
      `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))

(setq create-lockfiles nil)

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa") t)
(add-to-list 'package-archives '("elpa" . "https://elpa.gnu.org/packages") t)
(package-initialize)
(unless (package-installed-p 'use-package))
(require 'use-package)
(setq use-package-always-ensure t)

(use-package ivy
  :custom
  (ivy-initial-inputs-alist
   '((counsel-minor . "^+")
     (counsel-package . "^+")
     (counsel-org-capture . "^")
     (counsel-M-x . "")
     (counsel-describe-symbol . "^")
     (org-refile . "^")
     (org-agenda-refile . "^")
     (org-capture-refile . "^")
     (Man-completion-table . "^")
     (woman . "^")))
  :config (ivy-mode 1))

(use-package swiper)

(use-package ivy-rich
  :init
  (ivy-rich-mode 1)
  (setcdr (assq t ivy-format-functions-alist) #'ivy-format-function-line))

(use-package all-the-icons-ivy-rich
  :init (all-the-icons-ivy-rich-mode 1))

(use-package counsel
  :bind
  ("M-x" . counsel-M-x)
  ("C-x b" . counsel-ibuffer)
  ("C-x C-f" . counsel-find-file))

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config (setq which-key-idle-delay 0.05))

(use-package org-bullets
  :init (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
:custom
(org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

(defun custom/org-mode-visual-fill ()
  (setq visual-fill-column-width 100
	visual-fill-column-center-text t)
  (visual-fill-column-mode 1)
  (visual-line-mode 1)
  (auto-fill-mode 1))

(use-package visual-fill-column
  :hook (org-mode . custom/org-mode-visual-fill))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package drag-stuff
  :config
  (drag-stuff-global-mode 1)
  (drag-stuff-define-keys))

(use-package multiple-cursors)

(use-package toc-org
  :commands toc-org-enable
  :hook (org-mode . toc-org-enable))

(use-package doom-modeline
  :hook (after-init . doom-modeline-mode))
(setq doom-modeline-height 35)

(use-package projectile
  :config (projectile-mode)
  :custom (projectile-completion-system 'ivy)
  :init (when (file-directory-p "~/Projekty")
          (setq projectile-project-search-path '("~/Projekty")))
  (setq projectile-switch-project-action #'projectile-dired))

(use-package counsel-projectile
  :config (counsel-projectile-mode))

(use-package lsp-ui
  :hook (lsp-mode . lsp-ui-mode))

(use-package company
  :after lsp-mode
  :hook (prog-mode . company-mode)
  :bind (:map company-active-map
	      ("<tab>" . company-complete-selection))
  (:map lsp-mode-map
  ("<tab>" . company-indent-or-complete-common))
  :custom
  (company-minimum-prefix-length 1)
  (company-idle-delay 0.0))

(use-package company-box
  :hook (company-mode . company-box-mode))

(use-package treemacs
  :bind (:map treemacs-mode-map
	      ([mouse-1] . treemacs-single-click-expand-action))
  :config (treemacs-project-follow-mode t))

(use-package lsp-treemacs
  :config (lsp-treemacs-sync-mode 1))

(use-package dashboard
  :if (< (length command-line-args) 2)
  :init
  (setq initial-buffer-choice 'dashboard-open)
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  (setq dashboard-banner-logo-title "Welcome in Emacs!")
  (setq dashboard-startup-banner "~/.config/emacs/dashboard/banner/banner.txt")
  (setq dashboard-center-content t)
  (setq dashboard-items '((projects  . 3)
			  (recents   . 5)
			  (agenda    . 5)
			  (bookmarks . 3)))
  :config
  (dashboard-setup-startup-hook))

(use-package beacon
  :init(beacon-mode 1))

(use-package gptel
  :config
  (setq
   gptel-default-mode 'org-mode
    gptel-model "codellama:latest"
    gptel-backend (gptel-make-ollama "Ollama"
		    :host "localhost:11434"
		    :stream t
		    :models '("codellama:latest"))))

(use-package fzf)

(menu-bar-mode -1)

(tooltip-mode -1)

(tool-bar-mode -1)

(column-number-mode)

(global-display-line-numbers-mode t)

(use-package ivy-posframe
  :config
  (setq ivy-posframe-display-functions-alist
	`((counsel-find-file . ivy-posframe-display-at-window-center)
	  (counsel-M-x . ivy-posframe-display-at-frame-center)
	  (counsel-fzf . ivy-posframe-display-at-frame-center)))
  (ivy-posframe-mode 1))

(use-package catppuccin-theme
  :ensure
  :config (setq catppuccin-flavor 'frappe))
(load-theme 'catppuccin :no-confrim)

(set-frame-parameter nil 'alpha-background 70)
(add-to-list 'default-frame-alist '(alpha-background . 70))

(use-package treemacs-all-the-icons
  :config (treemacs-load-theme "all-the-icons"))

(setq org-ellipsis " "
      org-hide-emphasis-makers t)

(set-face-attribute
 'default nil
 :font "Fira Code Nerd Font"
 :weight 'bold)

(set-face-attribute
 'fixed-pitch nil
 :font "Fira Code Nerd Font"
 :weight 'bold)

(use-package ligature
  :config
  (ligature-set-ligatures 't '("www"))
  (ligature-set-ligatures 'eww-mode '("ff" "fi" "ffi"))
  (ligature-set-ligatures 'prog-mode '("|||>" "<|||" "<==>" "<!--" "####" "~~>" "***" "||=" "||>"
				       ":::" "::=" "=:=" "===" "==>" "=!=" "=>>" "=<<" "=/=" "!=="
				       "!!." ">=>" ">>=" ">>>" ">>-" ">->" "->>" "-->" "---" "-<<"
				       "<~~" "<~>" "<*>" "<||" "<|>" "<$>" "<==" "<=>" "<=<" "<->"
				       "<--" "<-<" "<<=" "<<-" "<<<" "<+>" "</>" "###" "#_(" "..<"
				       "..." "+++" "/==" "///" "_|_" "www" "&&" "^=" "~~" "~@" "~="
				       "~>" "~-" "**" "*>" "*/" "||" "|}" "|]" "|=" "|>" "|-" "{|"
				       "[|" "]#" "::" ":=" ":>" ":<" "$>" "==" "=>" "!=" "!!" ">:"
				       ">=" ">>" ">-" "-~" "-|" "->" "--" "-<" "<~" "<*" "<|" "<:"
				       "<$" "<=" "<>" "<-" "<<" "<+" "</" "#{" "#[" "#:" "#=" "#!"
				       "##" "#(" "#?" "#_" "%%" ".=" ".-" ".." ".?" "+>" "++" "?:"
				       "?=" "?." "??" ";;" "/*" "/=" "/>" "//" "__" "~~" "(*" "*)"
				       "\\\\" "://"))
  (global-ligature-mode t))

(set-face-attribute 'variable-pitch nil :family "SF Pro Text" :weight 'normal)
(set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
(set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
(set-face-attribute 'org-table nil   :inherit '(shadow fixed-pitch))
(set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
(set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
(set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
(set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch)

(use-package centaur-tabs
  :demand
  :custom
  (centaur-tabs-set-icons t)
  (centaur-tabs-height 20)
  (centaur-tabs-gray-out-icons 'buffer)
  (centaur-tabs-set-bar 'left)
  (centaur-tabs-close-button "✖")
  (centaur-tabs-set-modified-marker t)
  (centaur-tabs-show-navigation-buttons t)
  :hook
  (dashboard-mode . centaur-tabs-local-mode)
  (term-mode . centaur-tabs-local-mode)
  (calendar-mode . centaur-tabs-local-mode)
  (org-agenda-mode . centaur-tabs-local-mode)
  :config
  (centaur-tabs-mode t))

(electric-pair-mode t)

(use-package smooth-scrolling
  :config
  (smooth-scrolling-mode 1))

(global-unset-key (kbd "\C-d"))
(global-unset-key (kbd "\C-p"))
(global-unset-key (kbd "\C-a"))
(global-unset-key (kbd "\C-v"))
(global-unset-key (kbd "\M-v"))
(global-unset-key (kbd "\C-y"))
(global-set-key (kbd "<escape>") 'mc/keyboard-quit)
(global-set-key (kbd "C-<escape>") 'delete-window)
(global-set-key (kbd "M-x") 'counsel-M-x)
(global-set-key (kbd "C-f") 'swiper)
(global-set-key (kbd "C-k") 'kill-whole-line)
(global-set-key (kbd "C-d") 'duplicate-line)
(global-set-key (kbd "M-/") 'comment-line)
(global-set-key (kbd "C-z") 'mc/mark-next-word-like-this)
(global-set-key (kbd "C-.") 'company-complete)
(global-set-key (kbd "C-x k") 'kill-this-buffer)
(global-set-key (kbd "C-t") 'counsel-fzf)
(global-set-key (kbd "\C-V") 'yank)

(use-package general
  :config
  (general-create-definer vsstyle
    :prefix "C-P")
  (general-create-definer afro/agenda
    :prefix "C-a")

  (afro/agenda
    "a" '(org-agenda :wk "Agenda")
    "s" '(org-schedule :wk "Schedule")
    "d" '(org-deadline :wk "Deadline")
    "l" '((lambda () (interactive)(find-file "~/.config/emacs/agenda/Tasks.org")) :wk "Open task list")
    "j" '((lambda () (interactive)(find-file "~/.config/emacs/journal/Journal.org")) :wk "Open journal")
    "e" '((lambda () (interactive)(find-file "~/.config/emacs/agenda/Events.org")) :wk "Open journal")
    "t" '(org-set-tags-command :wk "Tag")
    "f" '(org-todo :wk "TODO Flag")
    "r" '(org-refile :wk "Archive task")
    "c" '(org-capture :wk "Capture")
    )

  (vsstyle 

    "a" '(:ignore t :wk "AI")
    "a c" '(gptel-system-prompt :wk "Change mode")
    "a o" '(gptel :wk "Open AI Window")
    "a s" '(gptel-send :wk "Ask AI")

    "b" '(:ignore t :wk "Buffer")
    "b m" '(counsel-switch-buffer :wk "Menu")

    "d" '(:ignore t :wk "Dir")
    "d c" '(treemacs-create-dir :wk "Create")
    "d d" '(treemacs-delete-file :wk "Delete")
    "d t" '(treemacs :wk "Tree")

    "e" '(:ignore t :wk "Emacs")
    "e d" '(dashboard-open :wk "Dashboard")
    "e s" '(scratch-buffer :wk "Scratch")

    "f" '(:ignore t :wk "File")
    "f c" '(treemacs-create-file :wk "Create")
    "f d" '(treemacs-delete-file :wk "Delete")
    "f f" '(counsel-projectile-grep :wk "Find text in project")
    "f r" '(:ignore t :wk "Reformat")
    "f r h" '(prettier-prettify :wk "Whole file")
    "f r r" '(prettier-prettify-region :wk "Selected region")

    "g" '(:ignore t :wk "Go to")
    "g d" '(lsp-ui-peek-find-definition :wk "Definition")
    "g i" '(lsp-ui-peek-find-definition :wk "Implementation")
    "g l" '(goto-line :wk "Line")
    "g r" '(lsp-ui-peek-find-references :wk "References")

    "p" '(:ignore t :wk "Project")
    "p f" '(counsel-fzf :wk "Switch file")
    "p s" '(projectile-switch-project :wk "Switch project")

    "r" '(:ignore t :wk "Run")
    "r c" '(compile :wk "Compile")
    "r t" '(org-babel-tangle :wk "Tangle")

    "s" '(:ignore t :wk "Settings")
    "s r" '(:ignore t :wk "Reload")
    "s r e" '((lambda () (interactive)(load-file "~/.config/emacs/init.el")) :wk "Emacs")
    "s r l" '(lsp-workspace-restart :wk "LSP")
    "s r p" '(prettier-restart :wk "Prettier")
    "s c" '((lambda () (interactive)(find-file "~/.config/emacs/config.org")) :wk "Open config file")

    "t" '(:ignore t :wk "Tab")
    "t n" '(centaur-tabs--create-new-tab :wk "New")

    "w" '(:ignore t :wk "Window")
    "w c" '(delete-window :wk "Close")
    "w s" '(:ignore t :wk "Split")
    "w s h" '(split-window-below :wk "Horizontal")
    "w s v" '(split-window-right :wk "Vertical")

    )
  )

(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :init (setq lsp-keymap-prefix "C-.")
  :config (lsp-enable-which-key-integration t))

(require 'org-tempo)

(add-hook 'org-mode-hook 'variable-pitch-mode)

(setq org-directory "~/.config/emacs/agenda")
  (setq org-agenda-files '("Tasks.org" "Birthdays.org" "Events.org"))
  (setq org-agenda-span 'week)
  (setq org-agenda-start-with-log-mode t)
  (setq org-log-done 'time)
  (setq org-log-into-drawer t)
  (setq org-todo-keywords
	  '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d!)")
	  (sequence "EVENT(e)" "CLOSED(q))" "|" "DONE(d!)")
	    (sequence "BACKLOG(b)" "PLAN(p)" "READY(r)" "ACTIVE(a)" "REVIEW(v)" "WAIT(w@/!)" "HOLD(h)" "|" "COMPLETED(c)" "CANC(k@)")))
  (setq org-refile-targets
	'(("Archive.org" :maxlevel . 1)))
  (setq org-refile-targets
      '(("Archive.org" :maxlevel . 1)))
  (advice-add 'org-refile :after 'org-save-all-org-buffers)

(setq org-capture-templates
  `(("t" "Tasks / Projects")
    ("tt" "Task" entry (file+olp "~/.config/emacs/agenda/Tasks.org" "Inbox")
	 "* TODO %?\n  %U\n  %a\n  %i" :empty-lines 1)
    ("te" "Event" entry (file+olp "~/.config/emacs/agenda/Events.org" "Active")
	 "* EVENT %?\n  %U\n  %a\n" :empty-lines 1)
    ("ts" "Clocked Entry Subtask" entry (clock)
	 "* TODO %?\n %U\n  %a\n  %i" :empty-lines 1)

    ("j" "Journal Entries")
    ("jj" "Journal" entry
	 (file+olp+datetree "~/.config/emacs/journal/Journal.org")
	 "\n* %<%H:%M %p> - Journal :journal:\n\n%?\n\n"
	 :clock-in :clock-resume
	 :empty-lines 1)
    ("jm" "Meeting" entry
	 (file+olp+datetree "~/.config/emacs/journal/Journal.org")
	 "* %<%H:%M %p> - %a :meetings:\n\n%?\n\n"
	 :clock-in :clock-resume
	 :empty-lines 1)))

(use-package web-mode
 :mode (("\\.ts\\'" . web-mode)
	("\\.js\\'" . web-mode)
	("\\.mjs\\'" . web-mode)
	("\\.tsx\\'" . web-mode)
	("\\.jsx\\'" . web-mode))
 :hook (web-mode . lsp-deferred)
 :config
 (setq web-mode-content-types-alist
       '(("jsx" . "\\.js[x]?\\'")))
 (setq web-mode-enable-auto-closing t)
 (setq web-mode-enable-auto-paring t))

(use-package eglot
  :hook
  (web-mode . eglot-ensure)
  (c-mode . eglot-ensure)
  (c++-mode . eglot-ensure))

(use-package yasnippet
  :init (yas-global-mode 1))

(use-package prettier)
(add-hook 'web-mode-hook (lambda () (add-hook 'before-save-hook 'prettier-prettify)))

(use-package flycheck
  :init (global-flycheck-mode))

(use-package highlight-indent-guides
  :custom
  (highlight-indent-guides-method 'bitmap)
  (highlight-indent-guides-responsive 'top)
  :hook (prog-mode . highlight-indent-guides-mode))
